#!/usr/bin/env php
<?php

use OpenapiFastroute\Parser;

$autoloadFound = false;
$autoloads = [
    __DIR__ . "/../../../autoload.php",
    __DIR__ . "/../vendor/autoload.php",
];
foreach ($autoloads as $autoload) {
    if (is_readable($autoload)) {
        include $autoload;
        $autoloadFound = true;
        break;
    }
}

if (!$autoloadFound) {
    echo "autoload.php not found, searched " . join(", ", $autoloads) . "!\n";
    exit(1);
}

$source = $_SERVER["argv"][1] ?? null;
if (empty($source) || $source == "-") {
    $fp = STDIN;
} else {
    $fp = fopen($source, "r");
}

if (!$fp) {
    echo "Cannot read source JSON\n";
    exit(1);
}

$json = "";
while (!feof($fp)) {
    $json .= fread($fp, 1024);
}

try {
    echo "<?php return " . var_export(Parser::fromJsonString($json)->parse(), 1) . ";";
} catch (\Throwable $e) {
    echo get_class($e) . ":\n";
    echo $e->getMessage()."\n";
    echo "Stack trace:\n";
    echo $e->getTraceAsString()."\n\n";
    exit(1);
}
