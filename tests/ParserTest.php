<?php

namespace Tests\OpenapiFastroute;

use OpenapiFastroute\Parser;

use OpenapiFastroute\RouteSpec;
use \PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{

    private $tmpFiles = [];

    public function testParse()
    {
        $json = <<<JSON
{
    "openapi": "3.0.1",
    "info": {
        "title": "Some title"
    },
    "paths": {
        "\\/": {
            "get": {
                "operationId": "IndexController@indexAction"
            }
        },
        "\\/endpoint\\/with\\/{parameter}": {
            "post": {
                "operationId": "EndpointController@someAction",
                "parameters": [{
                    "name": "id",
                    "in": "path",
                    "required": "true",
                    "schema": { "type": "integer" }
                }]
            }
        }
    }
}
JSON;
        $parser = Parser::fromJsonString($json);
        $specs = $parser->parse();
        $expectedSpecs = [
            new RouteSpec("/", "get", "IndexController@indexAction"),
            new RouteSpec("/endpoint/with/{parameter}", "post", "EndpointController@someAction"),
        ];
        /**
         * @var int $i
         * @var RouteSpec $expectedSpec */
        foreach ($expectedSpecs as $i => $expectedSpec) {
            $actual = $specs[$i];
            $this->assertEquals($expectedSpec->getUri(), $actual->getUri());
            $this->assertEquals($expectedSpec->getMethod(), $actual->getMethod());
            $this->assertEquals($expectedSpec->getOperationId(), $actual->getOperationId());
        }
    }

    /**
     * @dataProvider provideJsonStrings
     */
    public function testFromJsonString(string $json, string $exception = null)
    {
        if ($exception) {
            $this->expectException($exception);
        }

        $parser = Parser::fromJsonString($json);
        $this->assertInstanceOf(Parser::class, $parser);
    }

    /**
     * @dataProvider provideJsonFileContents
     */
    public function testFromJsonFile(string $json, string $exception = null)
    {
        if ($exception) {
            $this->expectException($exception);
        }

        $filename = sys_get_temp_dir() . "/" . uniqid() . ".json";
        $this->tmpFiles[] = $filename;
        file_put_contents($filename, $json);
        $parser = Parser::fromJsonFile($filename);
        $this->assertInstanceOf(Parser::class, $parser);
    }

    /**
     * @param array|null $parameter
     * @param array $expected
     * @dataProvider providerParseParameterData
     */
    public function testParseParameter(?array $parameter, array $expected)
    {
        $parser = new Parser([]);
        $route = new RouteSpec("/", "get", "test");
        $parser->parseParameter($parameter, $route);
        $this->assertEquals($expected, $route->getConstraints());
    }

    public function provideJsonStrings()
    {
        return [
            "valid" => ["{}", null],
            "empty" => ["", \JsonException::class],
            "invalid" => ["{", \JsonException::class],
            "not an array" => ["false", \TypeError::class],
        ];
    }

    public function provideJsonFileContents()
    {
        return [
            "valid" => ["{}", null],
            "empty" => ["", \InvalidArgumentException::class],
            "invalid" => ["{", \JsonException::class],
            "not an array" => ["false", \TypeError::class],
        ];
    }

    public function tearDown(): void
    {
        foreach ($this->tmpFiles as $tmpFile) {
            unlink($tmpFile);
        }
        $this->tmpFiles = [];
    }

    public function providerParseParameterData()
    {
        return [
            "no parameters" => [[], []],
            "integer" => [
                ["name" => "id", "in" => "path", "schema" => ["type" => "integer"]],
                ["id" => Parser::PATTERN_INT]
            ],
            "number" => [
                ["name" => "amount", "in" => "path", "schema" => ["type" => "number"]],
                ["amount" => Parser::PATTERN_NUMBER]
            ],
            "string with pattern" => [
                ["name" => "slug", "in" => "path", "schema" => ["type" => "string", "pattern" => "[a-z]+"]],
                ["slug" => "[a-z]+"]
            ],
            "string with min length" => [
                ["name" => "slug", "in" => "path", "schema" => ["type" => "string", "minLength" => 5]],
                ["slug" => ".{5,}"]
            ],
            "string with max length" => [
                ["name" => "slug", "in" => "path", "schema" => ["type" => "string", "maxLength" => 5]],
                ["slug" => ".{0,5}"]
            ],
            "string with max and min length" => [
                ["name" => "slug", "in" => "path", "schema" => ["type" => "string", "minLength" => 3, "maxLength" => 5]],
                ["slug" => ".{3,5}"]
            ],
            "string without constraints" => [
                ["name" => "slug", "in" => "path", "schema" => ["type" => "string"]],
                []
            ],
        ];
    }
}
