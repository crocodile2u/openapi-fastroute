<?php

declare(strict_types=1);

namespace OpenapiFastroute;

class RouteSpec
{
    /**
     * @var string
     */
    private $uri;
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $operationId;

    /**
     * @var string[]
     */
    private $constraints = [];

    private $omitSecurity = false;

    public function __construct(string $uri, string $method, string $operationId, array $constraints = [])
    {
        $this->uri = $uri;
        $this->method = $method;
        $this->operationId = $operationId;
        $this->constraints = $constraints;
    }

    /**
     * @return bool
     */
    public function omitSecurity(): bool
    {
        return $this->omitSecurity;
    }

    /**
     * @param bool $omitSecurity
     * @return $this
     */
    public function setOmitSecurity(bool $omitSecurity): RouteSpec
    {
        $this->omitSecurity = $omitSecurity;
        return $this;
    }

    public function constraint(string $name, string $regexp)
    {
        $this->constraints[$name] = $regexp;
    }

    /**
     * @return string[]
     */
    public function getConstraints(): array
    {
        return $this->constraints;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getOperationId(): string
    {
        return $this->operationId;
    }

    public static function __set_state(array $state)
    {
        return new self(
            $state["uri"] ?? "",
            $state["method"] ?? "",
            $state["operationId"] ?? "",
            $state["constraints"] ?? []
        );
    }
}