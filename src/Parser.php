<?php

declare(strict_types=1);

namespace OpenapiFastroute;

use function json_decode;

class Parser
{
    const PATTERN_INT = "[0-9]+";
    const PATTERN_NUMBER = "[0-9\.\+\-eE]+";
    /**
     * @var array
     */
    private $schema;

    /**
     * @param string $json
     * @throws \JsonException
     * @throws \TypeError
     */
    public static function fromJsonString(string $json)
    {
        $schema = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        return new self($schema);
    }

    /**
     * @param string $filename
     * @throws \InvalidArgumentException
     * @throws \JsonException
     */
    public static function fromJsonFile(string $filename)
    {
        if (!is_file($filename)) {
            throw new \InvalidArgumentException("$filename is not a file");
        }
        if (!is_readable($filename)) {
            throw new \InvalidArgumentException("$filename is not readable");
        }
        $json = file_get_contents($filename);
        if (empty($json)) {
            throw new \InvalidArgumentException("$filename is empty");
        }
        return self::fromJsonString($json);
    }

    public function __construct(array $schema)
    {
        $this->schema = $schema;
    }

    /**
     * @return RouteSpec[]
     */
    public function parse(): array
    {
        $ret = [];
        $basepath = $this->basepath();
        $paths = $this->schema["paths"] ?? [];
        foreach ($paths as $path => $methods) {
            foreach ($methods as $method => $spec) {
                $route = new RouteSpec("$basepath$path", $method, $spec["operationId"] ?? "");
                $security = $spec["security"] ?? null;
                $route->setOmitSecurity($security === []);
                foreach (($spec["parameters"] ?? []) as $parameter) {
                    $this->parseParameter($parameter, $route);
                }
                $ret[] = $route;
            }
        }
        return $ret;
    }

    public function parseParameter(array $parameter, RouteSpec $route): void
    {
        $name = $parameter["name"] ?? "";
        if (empty($name)) {
            return;
        }

        if ($parameter["in"] === "path") {
            $schema = $parameter["schema"] ?? null;
            if (empty($schema)) {
                return;
            }
            switch ($schema["type"] ?? null) {
                case "integer":
                    $route->constraint($name, self::PATTERN_INT);
                    break;
                case "number":
                    $route->constraint($name, self::PATTERN_NUMBER);
                    break;
                case "string":
                    $pattern = $schema["pattern"] ?? "";
                    if ($pattern) {
                        $route->constraint($name, $pattern);
                    } else {
                        $min = $schema["minLength"] ?? "";
                        $max = $schema["maxLength"] ?? "";
                        if ($min && $max) {
                            $route->constraint($name, sprintf(".{%d,%d}", $min, $max));
                        } elseif ($max) {
                            $route->constraint($name, sprintf(".{%d,%d}", 0, $max));
                        } elseif ($min) {
                            $route->constraint($name, sprintf(".{%d,}", $min));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private function basepath()
    {
        $url = $this->schema["servers"][0]["url"] ?? "";
        if ($url && ($url[0] != "/")) {
            return parse_url($url, PHP_URL_PATH);
        } else {
            return $url;
        }
    }
}